package com.zenika.medicineclash;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Medicine {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    @ManyToOne
    private Patient patient;

    @OneToMany(mappedBy = "medicine")
    private Collection<Prescription> prescriptions = new ArrayList<>();

    protected Medicine() {}

    public Medicine(String name) {
        this.name = name;
    }

    public void addPrescription(Prescription prescription) {
        this.prescriptions.add(prescription);
    }

    public String getName() {
        return name;
    }

    public Collection<Prescription> getPrescriptions() {
        return prescriptions;
    }
}
