package com.zenika.medicineclash;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@RestController
public class MedicineClashController {

    @Autowired
    private PatientRepository patientRepository;

    /**
     * Return the list of each days where patient was prescribed medicines matching <code>medicineNames</code>.
     *
     * @param patientId
     * @param medicineNames
     * @return
     */
    @PostMapping("/patient/{patientId}/clash")
    public List<LocalDate> clash(@PathVariable int patientId, @RequestBody List<String> medicineNames) {
        return patientRepository
                .findById(patientId)
                .map(patient -> patient.clash(medicineNames))
                .orElse(Collections.emptyList())
                .stream().toList();
    }
}
