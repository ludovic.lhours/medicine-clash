package com.zenika.medicineclash;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Patient {

    @Id
    @GeneratedValue
    private int id;

    @OneToMany(mappedBy = "patient")
    private Collection<Medicine> medicines = new ArrayList<>();

    public void addMedicine(Medicine medicine) {
        this.medicines.add(medicine);
    }

    public Collection<LocalDate> clash(Collection<String> medicineNames) {
        return clash(medicineNames, 90);
    }

    public Collection<LocalDate> clash(Collection<String> medicineNames, int daysBack) {
        throw new RuntimeException("TODO");
    }
}
