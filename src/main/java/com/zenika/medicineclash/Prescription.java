package com.zenika.medicineclash;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

import java.time.LocalDate;
import java.util.Date;

@Entity
public class Prescription {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private Medicine medicine;

    private LocalDate dispenseDate = LocalDate.now();
    private int daysSupply = 30;

    protected Prescription() {}

    public Prescription(LocalDate dispenseDate, int daysSupply) {
        this.dispenseDate = dispenseDate;
        this.daysSupply = daysSupply;
    }

    public LocalDate getDispenseDate() {
        return dispenseDate;
    }

    public int getDaysSupply() {
        return daysSupply;
    }
}
