INSERT INTO patient (id) VALUES (1);
INSERT INTO medicine (id, patient_id, name) VALUES (1, 1, 'Codeine');
INSERT INTO medicine (id, patient_id, name) VALUES (2, 1, 'Prozac');
INSERT INTO prescription (id, medicine_id, dispense_date, days_supply) VALUES (1, 1, '2024-11-01', 20);
INSERT INTO prescription (id, medicine_id, dispense_date, days_supply) VALUES (2, 2, '2024-11-10', 15);
